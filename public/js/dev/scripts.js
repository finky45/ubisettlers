/*
*	Modal logic section
*/

var closeModal = function() {
	$('#overlay').css('display','none');
	$('.scroll').css('display','none');
	$('.section').css('display','none');
}

$('#overlay').click(function(event){
	closeModal();
});

/*
*	Participate
*/

$('button#participate').click(function(event){
	event.preventDefault();
	$('#overlay').css('display','block');
	$('.scroll').css('display','block');
	$('.signup').css('display','block');
	bindValidator('signup');
});

/*
*	Prizes
*/

$('button#prizes').click(function(event){
	event.preventDefault();
	$('#overlay').css('display','block');
	$('.scroll').css('display','block');
	$('.prizes').css('display','block');
});

/*
*	Show Map
*/

// $('button#show-map').click(function(event){
// 	event.preventDefault();
// 	$('#overlay').css('display','block');
// 	$('.scroll').css('display','block');
// 	$('.map-popup').css('display','block');
// });

/*
*	Form validation section
*/

/* 
* This function binds the validator to the form
* It's necessary because the content of .signup is swapped out
* therefore the validator is still bound to the old elements
* which no longer exist
*/
var bindValidator = function(name) {
	new FormValidator(name, [{
	    name: 'postalcode',
	    rules: 'numeric|required',
	}, {
	    name: 'lastname',
	    rules: 'required',
	}, {
	    name: 'firstname',
	    rules: 'required',
	}, {
	    name: 'email',
	    rules: 'valid_email|required',
	}, {
	    name: 'terms',
	    rules: 'required',
	}], function(errors, event) {

		// Clear errors
		$('#postalcode-wrapper').removeClass('error');
		$('#lastname-wrapper').removeClass('error');
		$('#firstname-wrapper').removeClass('error');
		$('#email-wrapper').removeClass('error');
		$('#terms-wrapper').removeClass('error');

		$('.signup div#errors').empty();
		$('.signup div#errors').removeClass('visible');

		//errors.length = 0;

	    if (errors.length > 0) {
	    	//Handle errors
	        for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
	           console.log(errors[i].message);
	           	$('#'+errors[i].name+'-wrapper').addClass('error');
	           	$('.signup div#errors').append(errors[i].message + '<br />');
	           	$('.signup div#errors').addClass('visible');
	        }
	    }

	    if (event && event.preventDefault) {
	        event.preventDefault();
	    } else if (event) {
	        event.returnValue = false;
	    }

	   if(errors.length == 0) {

	    	var data = $('#signup').serialize();
			$.post($('form#signup').attr('action'), data, function(data){

				//console.log(data);
				$(".signup.section").html(data);
				bindValidator('signup');
				$('.signup.section').css('display','block');
			});
			
		}
	});
};

/*
*	Call this function when sending page to open
*/
var openMap = function() {
	$('#overlay').css('display','block');
	$('.scroll').css('display','block');
	$('.map-popup').css('display','block');
}