<!DOCTYPE html>
<html>
<head>
   <title>Admin Area</title>
</head>
<body>

<a href="../admin/logout">Logout</a>

@if(empty($users))
   There are no users...
@else
   <form method="POST" action="{{Request::url()}}/delete">
      <button type="button" id="select-all">Select All</button>
      <button type="reset" id="select-none">Select None</button>
      <button type="submit" value="delete" name="action" >Delete Selected</button>
      <table>
         <caption>Users</caption>
         <thead>
            <tr>
               <th></th>
               @foreach($users[0] as $attribute=>$value)
               <th>{{$attribute}}</th>
               @endforeach
            </tr>
         </thead>
         <tbody>
            @foreach($users as $user)
            <tr>
            <td>
               <input name="ids[]" class="delete-check" type="checkbox" value="{{$user['id']}}">
            </td>
               @foreach($user as $attribute=>$value)
                  <td>{{$value}}</td>
               @endforeach
            </tr>
            @endforeach
         </tbody>
      </table>
   </form>
@endif

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/admin.js"></script>

</body>
</html>