Hello {{$user['first_name']}},

Click <a href="{{route('confirmation', array($user['conf_code']))}}">here</a> to confirm your email address and validate your registration.