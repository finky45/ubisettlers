Hello {{$user['first_name']}},

This is a reminder that you did not validate your email and therefore your account is not yet valid. <br />
Please click <a href="{{route('confirmation', array($user['conf_code']))}}">here</a> to confirm your email address.