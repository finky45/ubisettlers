<!DOCTYPE html>
<html>
<head>
	<title>Settlers Online</title>
	<meta charset="utf-8">
	<meta name="description" content="Settlers Online">
	<meta name="keywords" content="free 2 play">
	<meta name="author" content="Paul Cristea">

	<meta name="viewport" content="width=device-width">
	
	<link rel="icon" href="" type="image/x-icon">
	<link rel="shortcut icon" href="" type="image/x-icon">
	<link rel="stylesheet" href="../css/main.css">
</head>
<body>
<div class="header"><div class="center"></div></div>
<div class="divider"></div>
<div id="main">
	<div class="logo"></div>
	<button class="home-link"><span>zur Homepage</span></button>
	<div class="map-bg"></div>
	<h1 class="subtitle1">Wo siedelt <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deutschland ? </h1>
	<h2 class="subtitle2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hilf uns, die fleissigste Siedler-Region in <br/> Deutschland zu finden und gewinne tolle Preise!</h2>
	<ul class="menu">
		<li>
			<button id="participate"><span>Mitmachen</span></button>
		</li>
		<li>
			<button id="prizes"><span>Preise Ansehen</span></button>
		</li>
		<li>
			<button id="tellfriend"><span>Weitersagen</span></button>
		</li>
	</ul>
	<div class="agb-link">Impressum <a href="#">AGB</a> Gewinnspielregeln</div>
	<button id="show-map" class="map-link"><span>Karte zeigen</span></button>
	<a class="fb-link" href="">
		<div class="white-box"><span>Jetzt ein Fan auf Facebook werden!</span></div>
		<div class="fb-logo"></div>
	</a>
	<div class="character">
		<?php $random_num = mt_rand(0,2); ?> 
		@if($random_num == 0)
			<img class="scholar" src="../img/scholar.png" alt="Scholar">
		@elseif($random_num == 1)
			<img class="merchant" src="../img/merchant.png" alt="Merchant">
		@else
			<img class="warrior" src="../img/warrior.png" alt="Warrior">
		@endif
	</div>
</div>

<footer>
	<div class="copyright">
		© 2009 Ubisoft Entertainment. Alle Rechte vorbehalten. Die Siedler, Blue Byte und das Blue Byte-Logo sind Warenzeichen von <br/>
		Red Storm Entertainment in den USA und/oder anderen Ländern. Nebelreich, Ubisoft und das Ubisoft-Logo sind Warenzeichen von Ubisoft Entertainment in den <br/>
		USA und/ oder anderen Ländern. Red Storm Entertainment Inc ist ein Unternehmen von Ubisoft Entertainment. Entwickelt von Blue Byte Software.
	</div>

	<ul class="logos">
		<li><a href="http://www.ubi.com/" class="ubi" title="Ubisoft"></a></li>
		<li><a href="#" class="dvd" title="PC-DVD"></a></li>
		<li><a href="http://www.bluebyte.com" class="bbyte" title="Blue Byte"></a></li>
	</ul>
</footer>

<div id="overlay"></div>
<div class="scroll">
	<div class="scroll-top"></div>
	<div class="scroll-mid">
		<div class="signup section">
			@include('landing.RegisterForm')
		</div>

		<div class="prizes section">
			<span class="place1">1. &amp; 2. PREIS:</span>
			<span class="place1desc">Gamer-Laptop "MSI GT640"<br/>
				Intel Core i7 QM720 (1,6 GHz)<br/>
				4 GByte DDR2<br/>
				Nvidia Geforce GTS 250M <br/>
				15,4-Zoll WSXGA (1680 x 1050)<br/>
				500 GByte SATA hdd (5400 rpm)<br/>
				3x USB, eSATA, HDMI, VGA, <br/>
				Ethernet, Modem, Webcam<br/>
			</span>
			<img class="place1" src="../img/msi-lap.png">
			<span class="place2">3.-12. PREIS:</span>
			<span class="place2desc">"Die Sieder 7 - Limitierte <br/>
				Edition" Mit Hochwertiger Figur,<br/>
				Castle Forge: (1x Gargoyle, 2x Fen-<br/>
				ster, 1x Torbogen, 2x Erker), <br/>
				Exklusive Map, Poster A2 und <br/>
				1 Packung Pflanzensamen<br/>
			</span>
			<img class="place2" src="../img/settlers-box.png">
			<button class="close" onClick="closeModal();"><span>zurück</span></button>
		</div>

		<div class="map-popup section">
			<span>Herzlichen Glückwunsch, Du hast Deine Postleitzahl zur Karte hinzugefügt und nimmst am Gewinnspiel teil. Viel Glück!</span>
			<img src="../img/confirm-map-de.png" alt="Confirm On Map">
			<button class="close" onClick="closeModal();"><span>schließen</span></button>
		</div>
	</div>
	<div class="scroll-bottom"></div>
</div>

<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript" src="../js/validate.min.js"></script>
<script type="text/javascript" src="../js/scripts.js"></script>

@if(isset($confirmed))
	<script>openMap()</script>
@else
@endif
</body>
</html>