<p>
	Wo wohnen unsere fleissigsten Siedler ? Um den besten Standort des geplanten Siedler-Live-Events zu finden, brauchen wir die hilfe der Community. Verratet uns, wo Ihr siedelt und gewinnt tolle Preise!
</p>
<form id="signup" name="signup" method="POST" action="/register">
	<fieldset>
		{{ Form::token(); }}
		<div id="postalcode-wrapper" class="{{{ isset($failed['postalcode']) ? 'error' : '' }}}">
			<label>Postleitzahl:
			<input name="postalcode" type="text"/ value="{{{ $oldvalues['postalcode'] or '' }}}">
			</label>
		</div>
		<div id="lastname-wrapper" class="{{{ isset($failed['lastname']) ? 'error' : '' }}}">
			<label>Vorname:
			<input name="lastname" type="text" value="{{{ $oldvalues['lastname'] or '' }}}"/>
			</label>
		</div>
		<div id="firstname-wrapper" class="{{{ isset($failed['firstname']) ? 'error' : '' }}}">
			<label>Nachname:
			<input name="firstname" type="text" value="{{{ $oldvalues['firstname'] or '' }}}"/>
			</label>
		</div>
		<div id="email-wrapper" class="{{{ isset($failed['email']) ? 'error' : '' }}}">
			<label>E-Mail:
			<input name="email" type="email" value="{{{ $oldvalues['email'] or '' }}}"/>
			</label>
		</div>
		
		<div id="terms-wrapper" class="{{{ isset($failed['terms']) ? 'error' : '' }}}">
			<label class="terms">
				<input name="terms" type="checkbox" />
				<div class="checkbox" value="checked"><span class="check">X</span></div>
				<span class="label">Ich habe die AGBs und die Teilnahmebedingen gelesen und stimme zu.</span>
			</label>
		</div>
		<div id="errors" class="errors @if(isset($errors) && $errors->count()) {{'visible'}} @endif">
			@if(isset($errors))
				@foreach($errors->all() as $error)
					{{ $error }} <br />
				@endforeach
			@endif
		</div>
		<input name="submit" type="submit" class="submit" title="Sign Up" value="abschicken">

	</fieldset>
</form>