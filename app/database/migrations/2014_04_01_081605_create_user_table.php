<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			/* InnoDB: Faster at writes (we'll mostly insert into this table) */
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('first_name', 50);
			$table->string('last_name', 50);

			/* Postal codes in Germany are 5 digits long. I left this as 10 in case it's used for other countries. Besure to modify filters.php as well*/
			$table->string('postal_code', 10);
			$table->string('email', 30);
			$table->boolean('confirmed')->default(false);
			$table->boolean('reminded')->default(false);
			$table->string('conf_code', '30');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
