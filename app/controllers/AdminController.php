<?php

class AdminController extends \BaseController {

	public static $admin_secret="FC9JDSPA";

	/**
	 * Display the main admin page.
	 *
	 * @return Response
	 */
	public function index()
	{

		$users = User::all();

		return View::make('Admin', array('users'=>$users->toArray()));
	}

	/**
	 * Display the login page
	 *
	 * @return Response
	 */

	public function login()
	{
		return View::make('admin.Login');
	}

	/**
	 * Force logout and return to root.
	 *
	 * @return Response
	 */
	public function logout()
	{
		Session::put('secret', '');
		return Redirect::to('/');
	}

	/**
	 * Attempt to log in from login page form.
	 *
	 * @return Response
	 */

	public function auth()
	{

		if(Input::get('secret') == self::$admin_secret) {
			Session::put('secret', self::$admin_secret);

			return Redirect::to('admin');
		}

		return Redirect::to('admin/login');
	}

	/**
	 * Remove the selected users
	 * Draws user ids from input
	 *
	 * @return Response
	 */
	public function delete()
	{
		User::destroy(Input::get('ids'));

		return Redirect::to('admin');
	}

}