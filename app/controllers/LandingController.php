<?php

class LandingController extends \BaseController {

	/**
	 * Display the landing page as seen for the first time.
	 *
	 * @return View
	 */
	public function index()
	{
		// Random number between 0-2 to display a random character on landing page.
		return View::make('Landing');
	}

	/**
	 * Create a new user row with the form data
	 * It is assumed by now, the XSS and CRSF filder have been included in routes,
	 * so we won't be sanitizing anything here.
	 * @return Response
	 */
	public function create_user()
	{

		/* Generate random string for a confirmation code. */
		$conf_code = str_random(30);

		/* Create a new user in the database */
		$new_user = User::create(array(
			'first_name'=>Input::get('firstname'),
			'last_name'=>Input::get('lastname'),
			'postal_code'=>Input::get('postalcode'),
			'email'=> strip_tags(Input::get('email')), //use strip tags in case of <script> or <?php tags
			'conf_code'=>$conf_code
		));

		/* Send confirmation email using the Confirmation view in emails/ */
		Mail::send('emails.Confirmation', array('user'=>$new_user->toArray()), function($message) use ($new_user)
		{
		    $message->to($new_user->email);
		    $message->subject('Your registration needs confirmation');
		});

		return View::make('landing.Confirmation');
	}

	/**
	 * Show the form with errors included.
	 *
	 * @return View
	 */
	public function show_form($errors, $failed)
	{
		
		$old_values = Input::all();
		return View::make('landing.RegisterForm', array('oldvalues'=>$old_values,'errors'=>$errors, 'failed'=>$failed));
	}

	/**
	 * Attempts to confirm the registration code
	 * and sends the user to a confirmation success page (map popup)
	 * or the landing page if it's not valid.
	 *
	 * @return View
	 */
	public function confirm($registration_code)
	{
		//

		$user = User::where('conf_code', '=', $registration_code)->get(array('id','first_name'))->first();
		if(!$user) {
			return View::make('Landing');
		}

		User::where('id', '=', $user->id)->update(array('confirmed' => true));

		return View::make('Landing', array('confirmed'=>true, $user));
	}


}