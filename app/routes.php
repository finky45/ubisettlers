<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Landing page */
Route::get('/', array('as'=>'landing', 'uses' => 'LandingController@index'));

/* Register form action, requires token against csrf and verifies register fields,
| see filters.php for field verification.
 */
Route::post('register', array('before' => array('csrf|register'), 'uses' => 'LandingController@create_user'));

/* Register confirmation */
Route::get('confirm/{conf_code}', array('as' => 'confirmation', 'uses' => 'LandingController@confirm'));

/* Admin Backend */

Route::get('admin/login', array('as'=>'admin.login', 'uses' => 'AdminController@login'));
Route::post('admin/auth', array('as'=>'admin.auth', 'uses' => 'AdminController@auth'));

Route::group(array('before' => 'admin'), function()
{
    Route::any('admin', array('as'=>'admin', 'uses' => 'AdminController@index'));
    Route::post('admin/delete', array('as'=>'admin.delete', 'uses' => 'AdminController@delete'));
    Route::get('admin/logout', array('as'=>'admin.logout', 'uses' => 'AdminController@logout'));
});