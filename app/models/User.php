<?php

class User extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes protected from mass assignment
	 *
	 * @var array
	 */
	protected $guarded = array('created_at', 'updated_at');
}