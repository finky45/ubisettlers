<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/



App::before(function($request)
{
	/*
	* This filter below will allow spaces and unicode characters
	*/
	Validator::extend('alpha_spaces', function($attribute, $value, $parameters)
	{
		return preg_match('/^[\pL\s]+$/u', $value);
	});
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		return App::make('LandingController')->show_form(null, null);
	}
});

/*
|--------------------------------------------------------------------------
| Register Form Filter
|--------------------------------------------------------------------------
|
| This filter is customized for the register form on the Landing view.
| It will validate all fields coming in from the register form.
| Postal codes in Germany are 5 digits.
| First names and last names are restricted to 3 characters minimum, up to 50.
| Email... basic rules apply (@ followed by .) and restricted to 30 chars.
| Also verifies the TOS checkbox is checked.
| For a complete list of validation rules: http://laravel.com/docs/validation
|
*/

Route::filter('register', function()
{
	$rules = array(
    	'postalcode' => 'required|integer|digits_between:5,5',
    	'firstname' => 'alpha_spaces|required|min:2,max:50',
    	'lastname' => 'alpha_spaces|required|min:2,max:50',
        'email' => 'required|email|unique:users',
        'terms' => 'required'
    );

	$validator = Validator::make(Input::all(), $rules );

	if($validator->fails()) {

		//A MessageBag instance of error messages
	 	$errors = $validator->messages();

	 	//An array of fields that have failed and why
	 	$failed = $validator->failed();

	 	return App::make('LandingController')->show_form($errors, $failed);
	}
});


Route::filter('admin', function()
{
 
	if(Session::get('secret') != AdminController::$admin_secret) {
		return Redirect::to('admin/login');
	}
});