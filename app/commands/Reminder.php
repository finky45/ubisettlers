<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Reminder extends Command {

	/*
	* After what time the reminder email will be sent to each user.
	* Use regular DateTime modifiers like '-1 day -23 hours -59 minutes -59 seconds'
	* Important: must use a negative value, because it's being substracted from the current time
	* For example if you want them to receive the email after seven hours, then set $send_after = '-7 hours';
	*/
	private static $send_after = '-1 hours';

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:sendReminders';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends verification reminder emails to participants.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $cutoff_time = new DateTime();
        $cutoff_time->modify(self::$send_after);
		/* 
		* Grab a list of users to send reminders to
		* Important: id and reminded are required to update the row
		* 
		*/
		$users = User::where('confirmed', '=', false)->where('reminded', '=', false)->where('created_at', '<', $cutoff_time)->get(array('id','reminded','first_name', 'email', 'conf_code'));

		foreach($users as $user) {

			/* Send confirmation email using the ConfirmationReminder view in emails/ */
			// Log::info('Sending reminder mail to '.$user->email.'\n');
			Mail::send('emails.Reminder', array('user'=>$user->toArray()), function($message) use ($user)
			{
			    $message->to($user->email);
			    $message->subject('Reminder: Your account is unverified.');
			});


			// modify and save the user
			$user->reminded = true;
			$user->save();
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}
