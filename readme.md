## Settlers Example Page

Installation Instructions:

After unzipping into webroot:
1. Edit app/config/app.php:
-line 29: url. Set this to the url of the site. Reminder emails depends on this to create correct activation URL.
2. Edit MySQL database credentials in app/config/database.php on line 55 to line 65.
3. Edit app/config/mail.php for mail server settings.
4. There is no database to import. Run "php artisan app:install" in webroot. The DB account needs CREATE priviledges for tables.
5. The email cron command is "php artisan command:sendReminders". Reminder delay is set to 1 hour and it can be changed in app/commands/Reminder.php.
6. The web root needs to be set as /public

The code for the admin backend is: FC9JDSPA
